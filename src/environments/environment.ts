// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDA3qdkO_HKe4fIpv0Pt8G_yQLYUO5f5_w",
    authDomain: "angularchat-9b3f9.firebaseapp.com",
    databaseURL: "https://angularchat-9b3f9.firebaseio.com",
    projectId: "angularchat-9b3f9",
    storageBucket: "angularchat-9b3f9.appspot.com",
    messagingSenderId: "574536246370",
    appId: "1:574536246370:web:63c589e3d494979c1edd3b"
  }
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
