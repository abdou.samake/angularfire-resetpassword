import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserLoginComponent} from "./auth/user-login/user-login.component";
import {UserInfoComponent} from "./auth/user-info/user-info.component";

const routes: Routes = [
  {path: '', redirectTo: '/user-login', pathMatch: 'full'},
  {path: 'user-login', component: UserLoginComponent},
  {path: 'user-info', component: UserInfoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
